package gamebase

import (
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/maoka/maoka-base/service"
	"gitlab.com/maoka/tool-kit/peer"
)

const (
	SessionPlayer = "player"
)

type Player interface {
	Send(string, interface{})
	Close()
	BindSession(string, *peer.Session, func(*peer.Session))
	GetNickName() string
	GetUserID() string
	GetPlayerID() uint64
	GetGameTime() time.Duration
	GetSessionID() (int64, bool)
}

type BaseUserInfo struct {
	PlayerID uint64
	UserName string
	Sex      uint8  //性别
	NickName string //昵称
	ImageURL string //头像
}

type BasePlayer struct {
	session   *peer.Session
	UserInfo  *BaseUserInfo
	Logger    *logrus.Entry
	loginTime time.Time
	IsVisitor bool
	IsRobot   bool
	sessMutex sync.Mutex
}

//发送消息到客户端
func (lp *BasePlayer) Send(router string, data interface{}) {
	lp.sessMutex.Lock()
	defer lp.sessMutex.Unlock()
	service.Send(lp.session, router, data)
}

func (lp *BasePlayer) GetSessionID() (int64, bool) {
	lp.sessMutex.Lock()
	defer lp.sessMutex.Unlock()
	if lp.session != nil {
		return lp.session.Conn.ID(), true
	}
	return 0, false
}

//关闭连接
func (lp *BasePlayer) Close() {
	lp.sessMutex.Lock()
	defer lp.sessMutex.Unlock()
	lp.session.Conn.Close()
}

//绑定链接
func (lp *BasePlayer) BindSession(userid string, session *peer.Session, cFunc func(*peer.Session)) {
	lp.sessMutex.Lock()
	defer lp.sessMutex.Unlock()
	if lp.session != nil {
		if lp.session.Conn.ID() == session.Conn.ID() {
			return
		}
		lp.session.Delete(SessionPlayer)
		oldSess := lp.session
		if cFunc != nil {
			cFunc(oldSess)
		}
		time.AfterFunc(time.Second*3, func() { //重复登录后三秒关闭
			oldSess.Conn.Close()
		})
	} else {
		lp.loginTime = time.Now()
	}
	lp.session = session
	lp.session.Set(SessionPlayer, userid)
	return
}

//获取显示ID
func (lp *BasePlayer) GetNickName() string {
	return lp.UserInfo.NickName
}

//获取账号ID
func (lp *BasePlayer) GetUserID() string {
	return lp.UserInfo.UserName
}

//获取游戏中生成ID
func (lp *BasePlayer) GetPlayerID() uint64 {
	return lp.UserInfo.PlayerID
}

func (lp *BasePlayer) GetGameTime() time.Duration {
	return time.Since(lp.loginTime)
}

//从链接获取玩家信息
func GetBindPlayerID(session *peer.Session) string {
	if playerID, ok := session.Get(SessionPlayer); ok {
		if p, ok := playerID.(string); ok {
			return p
		}
	}
	return ""
}
