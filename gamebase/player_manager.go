package gamebase

import "sync"

// var playerManager PlayerManager

type PlayerManager struct {
	playerList sync.Map //玩家列表
	//chKickOut  chan string //踢人
}

//广播给本服所有玩家
func (pm *PlayerManager) BroadCast(router string, msg interface{}) {
	pm.playerList.Range(func(user interface{}, value interface{}) bool {
		value.(Player).Send(router, msg)
		return true
	})
}

//广播给本服所有玩家 个别人除外
func (pm *PlayerManager) BroadCastExcept(router string, msg interface{}, userid string) {
	pm.playerList.Range(func(user interface{}, value interface{}) bool {
		if user.(string) == userid {
			return true
		}
		value.(Player).Send(router, msg)
		return true
	})
}

//遍历所有玩家
func (pm *PlayerManager) Range(f func(user interface{}, value interface{}) bool) {
	pm.playerList.Range(f)
}

//发送消息给部分玩家
func (pm *PlayerManager) SendTo(router string, msg interface{}, userid ...string) {
	for _, user := range userid {
		if user == "" {
			continue
		}
		if v, ok := pm.playerList.Load(user); ok {
			v.(Player).Send(router, msg)
		}
	}
}

//删除玩家
func (pm *PlayerManager) RemovePlayer(userid string) {
	if userid == "" {
		return
	}
	pm.playerList.Delete(userid)
}

//添加玩家到列表
func (pm *PlayerManager) AddPlayer(userid string, player Player) {
	if player == nil || userid == "" {
		panic("playermanager: add player failed empty userid or player")
	}
	pm.playerList.Store(userid, player)
}

//获取玩家
func (pm *PlayerManager) LoadPlayer(userid string) Player {
	if player, ok := pm.playerList.Load(userid); ok {
		return player.(Player)
	}
	return nil
}

func (pm *PlayerManager) Count(filter func(player Player) bool) int {
	var count int
	pm.playerList.Range(func(user interface{}, value interface{}) bool {
		if filter != nil {
			if ok := filter(value.(Player)); ok {
				count++
			}
		} else {
			count++
		}
		return true
	})
	return count
}
