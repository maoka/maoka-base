package alarmhook

import (
	"fmt"
	"os"
	"runtime"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/maoka/tool-kit/utils"
)

type AlarmHook struct {
	alarmAddr string
	formatter logrus.Formatter
}

func (h *AlarmHook) Fire(e *logrus.Entry) error {
	dataBytes, err := h.formatter.Format(e)
	if err != nil {
		return err
	}
	postContent := string(dataBytes)
	fmt.Println(postContent)
	_, err = utils.DoPost(h.alarmAddr, "", postContent, time.Second*6)
	return err
}

func (h *AlarmHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
	}
}

func New(alarmAddr, appname string) logrus.Hook {
	host, err := os.Hostname()
	if err != nil {
		host = "unknown"
	}
	return &AlarmHook{
		alarmAddr: alarmAddr,
		formatter: DefaultFormatter(logrus.Fields{
			"appname": appname,
			"version": "1",
			"type":    "logrus",
			"host":    host,
		}),
	}
}

var entryPool = sync.Pool{
	New: func() interface{} {
		return &logrus.Entry{}
	},
}

func releaseEntry(e *logrus.Entry) {
	entryPool.Put(e)
}

var (
	logrusFieldMap = logrus.FieldMap{
		logrus.FieldKeyTime: "timestamp",
		logrus.FieldKeyMsg:  "message",
		logrus.FieldKeyFunc: "funcname",
		logrus.FieldKeyFile: "filename",
	}
)

type AlarmFormatter struct {
	logrus.Formatter
	logrus.Fields
}

func DefaultFormatter(fields logrus.Fields) logrus.Formatter {
	return &AlarmFormatter{
		Formatter: &logrus.JSONFormatter{
			CallerPrettyfier: func(f *runtime.Frame) (string, string) {
				// s := strings.Split(f.Function, ".")
				// funcname := s[len(s)-1]
				// _, filename := path.Split(f.File)
				return f.Function, fmt.Sprintf("%s %d", f.File, f.Line)
			},
			FieldMap: logrusFieldMap,
		},
		Fields: fields,
	}
}

func copyEntry(e *logrus.Entry, fields logrus.Fields) *logrus.Entry {
	ne := entryPool.Get().(*logrus.Entry)
	*ne = *e
	for k, v := range fields {
		ne.Data[k] = v
	}
	return ne
}

func (f *AlarmFormatter) Format(e *logrus.Entry) ([]byte, error) {
	ne := copyEntry(e, f.Fields)
	dataBytes, err := f.Formatter.Format(ne)
	return dataBytes, err
}
