package log

import (
	"math"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mssola/user_agent"
	"github.com/sirupsen/logrus"
)

func Logger(logEntry *logrus.Entry, withUA bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		start := time.Now()
		c.Next()
		stop := time.Since(start)
		latency := int(math.Ceil(float64(stop.Nanoseconds()) / 1000000.0))
		statusCode := c.Writer.Status()
		clientIP := c.ClientIP()
		hostname, err := os.Hostname()
		if err != nil {
			hostname = "unknown"
		}
		var errMsg string
		if em, ok := c.Get("err_msg"); ok {
			errMsg = em.(string)
		}
		var entry = logEntry.WithFields(logrus.Fields{
			"hostname":   hostname,
			"statusCode": statusCode,
			"errMsg":     errMsg,
			"latency":    latency, // time to process
			"client":     clientIP,
			"path":       path,
		})
		if withUA {
			clientUserAgent := c.Request.UserAgent()
			referer := c.Request.Referer()
			ua := user_agent.New(clientUserAgent)
			engineName, engineVersion := ua.Engine()
			browserName, browserVersion := ua.Browser()
			entry = entry.WithFields(logrus.Fields{
				"clientIP":           clientIP,
				"referer":            referer,
				"userAgent":          clientUserAgent,
				"ua-mobile":          ua.Mobile(),
				"ua-bot":             ua.Bot(),
				"ua-mozilla":         ua.Mozilla(),
				"ua-platform":        ua.Platform(),
				"ua-os":              ua.OS(),
				"ua-engine-name":     engineName,
				"ua-engine-version":  engineVersion,
				"ua-browser-name":    browserName,
				"ua-browser-version": browserVersion,
			})
		}
		if len(c.Errors) > 0 {
			entry.Warn(c.Errors.ByType(gin.ErrorTypePrivate).String())
		} else {
			entry.Info("")
		}
	}
}
