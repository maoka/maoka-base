package log

import (
	"io/ioutil"
	"path"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

var _logger = logrus.New()
var logger *logrus.Entry

func init() {
	_logger.SetReportCaller(true)
	_logger.Formatter = &logrus.JSONFormatter{
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			s := strings.Split(f.Function, ".")
			funcname := s[len(s)-1]
			_, filename := path.Split(f.File)
			return funcname, filename
		},
	}
	_logger.SetLevel(logrus.InfoLevel)
}

// 日志初始化 应用名和输出标记 日志等级
func InitLogger(appName string, console bool, level logrus.Level, hooks ...logrus.Hook) {
	if logger == nil {
		logger = _logger.WithField("appname", appName)
	} else {
		*logger = *(_logger.WithField("appname", appName))
	}
	_logger.SetLevel(level)
	for _, v := range hooks {
		if v != nil {
			_logger.AddHook(v)
		}
	}
	if console == false {
		_logger.SetOutput(ioutil.Discard)
	}
}

func GetLogger() *logrus.Entry {
	if logger == nil {
		logger = _logger.WithField("package", "main")
	}
	return logger
}
