package alipay

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/maoka/maoka-base/log"
	"gitlab.com/maoka/tool-kit/utils"

	"gitlab.com/maoka/tool-kit/sign"
	_ "gitlab.com/maoka/tool-kit/sign/rsasha256"
)

var logger = log.GetLogger().WithField("package", "mkpay.alipay")

func generateSign(content string, signType string, key []byte) (string, error) {
	signObj := sign.GetSignObject(signType)
	if signObj == nil {
		return "", fmt.Errorf("sign type [%s] not register", signType)
	}
	return signObj.Sign([]byte(content), key)
}

func CheckSign(v url.Values, key []byte) error {
	signStr := v.Get("sign")
	v.Del("sign")
	v.Del("sign_type")
	signContent, err := url.QueryUnescape(v.Encode())
	if err != nil {
		return err
	}
	logger.Debug(signContent)
	return checkRSA2Sign([]byte(signContent), key, signStr)
}

func checkRSA2Sign(content, key []byte, signStr string) error {
	signObj := sign.GetSignObject("rsasha256")
	if signObj == nil {
		return fmt.Errorf("sign type rsasha256 not register")
	}
	return signObj.Verify(content, key, signStr)
}

type AlipayFundTransToaccountTransferResponse struct {
	Data struct {
		Code     string `json:"code"`
		Msg      string `json:"msg"`
		SubCode  string `json:"sub_code,omitempty"`
		SubMsg   string `json:"sub_msg,omitempty"`
		OutBizNo string `json:"out_biz_no,omitempty"`
		OrderId  string `json:"order_id,omitempty"`
		PayDate  string `json:"pay_date,omitempty"`
	} `json:"alipay_fund_trans_toaccount_transfer_response"`
	Sign string `json:"sign"`
}

type Alipay struct {
	AppId      string //商户ID
	PrivateKey []byte //私钥
	PublicKey  []byte //支付平台公钥
	NotifyUrl  string //通知接口
	ExtraParam string //回传参数
}

func (ap *Alipay) Transfer(amount float64, account, outTradeNo, desc, clientIp string) (string, string, error) {
	bizData := map[string]interface{}{
		"out_biz_no":      outTradeNo,
		"payee_type":      "ALIPAY_LOGONID",
		"payee_account":   account,
		"payer_show_name": desc,
		"amount":          fmt.Sprintf("%.02f", amount),
	}
	bizByte, err := json.Marshal(bizData)
	if err != nil {
		return "", "", err
	}
	v := url.Values{}
	v.Add("app_id", ap.AppId)
	v.Add("method", "alipay.fund.trans.toaccount.transfer")
	v.Add("charset", "utf-8")
	v.Add("timestamp", time.Now().Format("2006-01-02 15:04:05"))
	v.Add("version", "1.0")
	v.Add("sign_type", "RSA2")
	v.Add("biz_content", string(bizByte))
	signContent, _ := url.QueryUnescape(v.Encode())
	s, err := generateSign(signContent, "rsasha256", ap.PrivateKey)
	if err != nil {
		return "", "", err
	}
	v.Add("sign", s)
	logger.Debugf("alipay request:%s sign:%s", signContent, s)
	resp, err := utils.DoGet("https://openapi.alipay.com/gateway.do", v, time.Second*6)
	if err != nil {
		return "", "", fmt.Errorf("alipay: transfer do get failed err:%v", err)
	}
	logger.Debugf("alipay response:%s", string(resp))
	var baseResp AlipayFundTransToaccountTransferResponse
	err = json.Unmarshal(resp, &baseResp)
	if err != nil {
		return "", "", fmt.Errorf("alipay: transfer response unmarshal failed resp:%s err:%v", string(resp), err)
	}
	respSignCtx, err := sign.Object2OrderJson(&baseResp.Data)
	if err != nil {
		if err != nil {
			return "", "", fmt.Errorf("alipay: transfer object to order json failed resp:%s err:%v", string(resp), err)
		}
	}
	err = checkRSA2Sign(respSignCtx, ap.PublicKey, baseResp.Sign)
	if err != nil {
		return "", "", fmt.Errorf("alipay: transfer check sign failed resp:%s err:%v", string(resp), err)
	}
	// var respBizData map[string]string
	// err = json.Unmarshal([]byte(baseResp["alipay_fund_trans_toaccount_transfer_response"]), &respBizData)
	// if err != nil {
	// 	return "", "", fmt.Errorf("alipay: transfer alipay_fund_trans_toaccount_transfer_response unmarshal failed resp:%s err:%v", baseResp["alipay_fund_trans_toaccount_transfer_response"], err)
	// }
	if baseResp.Data.Code != "10000" {
		return "", "", fmt.Errorf("code[%s] sub_code[%s] err_code[%s]", baseResp.Data.Code, baseResp.Data.SubCode, baseResp.Data.SubMsg)
	}
	return baseResp.Data.OrderId, "SUCCESS", nil
}

func (ap *Alipay) Pay(amount float64, subject, body, outTradeNo, clientIp string) (string, string, error) {
	bizData := map[string]interface{}{
		"out_trade_no":    outTradeNo,
		"total_amount":    fmt.Sprintf("%.02f", amount),
		"subject":         subject,
		"body":            body,
		"product_code":    "QUICK_WAP_WAY",
		"passback_params": ap.ExtraParam,
	}
	bizByte, err := json.Marshal(bizData)
	if err != nil {
		return "", "", err
	}
	v := url.Values{}
	v.Add("app_id", ap.AppId)
	v.Add("method", "alipay.trade.wap.pay")
	v.Add("charset", "utf-8")
	v.Add("timestamp", time.Now().Format("2006-01-02 15:04:05"))
	v.Add("version", "1.0")
	v.Add("sign_type", "RSA2")
	v.Add("notify_url", ap.NotifyUrl)
	v.Add("biz_content", string(bizByte))
	signContent, _ := url.QueryUnescape(v.Encode())
	s, err := generateSign(signContent, "rsasha256", ap.PrivateKey)
	if err != nil {
		return "", "", fmt.Errorf("alipay: pay generate sign failed err:%v", err)
	}
	v.Add("sign", s)
	logger.Debugf("alipay request:%s sign:%s", signContent, s)
	payUrl, err := url.Parse("https://openapi.alipay.com/gateway.do" + "?" + v.Encode())
	logger.Debugf("alipay response:%s", payUrl)
	if err != nil {
		return "", "", fmt.Errorf("alipay: pay parse url failed err:%v", err)
	}
	return "", payUrl.String(), nil
}

func NewAlipay(extraParam, appId string, privatekey, publickey []byte, notifyUrl string) *Alipay {
	return &Alipay{
		AppId:      appId,
		PrivateKey: privatekey,
		PublicKey:  publickey,
		NotifyUrl:  notifyUrl,
		ExtraParam: extraParam,
	}
}
