package mkpay

type MKPay interface {
	//支付支付方orderid 跳转支付链接 错误信息
	Pay(amount float64, subject, body, outTradeNo, clientIp string) (string, string, error)
	Transfer(amount float64, account, outTradeNo, desc, clientIp string) (string, string, error)
}
