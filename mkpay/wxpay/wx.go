package wxpay

import (
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"reflect"
	"sort"
	"strings"
	"time"

	"gitlab.com/maoka/maoka-base/log"
	"gitlab.com/maoka/tool-kit/utils"
)

var logger = log.GetLogger().WithField("package", "mkpay.wx")

func struct2XmlTagMap(data interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	elem := reflect.ValueOf(data).Elem()
	for i := 0; i < elem.NumField(); i++ {
		field := elem.Type().Field(i).Tag.Get("xml")
		value := elem.Field(i).Interface()
		result[field] = value
	}
	return result
}

//微信支付签名
func wxGenerateSign(m utils.StringMap, key string) string {
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var signStrings string
	for _, k := range keys {
		value := fmt.Sprintf("%v", m[k])
		if value != "" && k != "sign" {
			signStrings = fmt.Sprintf("%s%s=%s&", signStrings, k, value)
		}
	}
	if key != "" {
		signStrings = fmt.Sprintf("%skey=%s", signStrings, key)
	}
	logger.Debugf("wx pay sign string:%s", signStrings)
	return strings.ToUpper(utils.GetMD5(signStrings))
}

//调用微信接口
func callWxPayAPI(api string, m utils.StringMap, key, mchid string, pemkey, cert []byte) (utils.StringMap, error) {
	m["sign"] = wxGenerateSign(m, key) //这个是计算wxpay签名的函数上面已贴出
	bytes_req, err := xml.MarshalIndent(m, "", "  ")
	if err != nil {
		return nil, err
	}
	str_req := string(bytes_req)
	str_req = strings.Replace(str_req, "StringMap", "xml", -1)
	logger.Debugf("wx pay request:%s", str_req)
	var resp []byte
	if mchid == "" {
		resp, err = utils.DoPost("https://api.mch.weixin.qq.com"+api, "application/xml;charset=utf-8", str_req, time.Second*6)
		if err != nil {
			return nil, fmt.Errorf("wx pay do post failed err:%v", err)
		}
	} else {
		dCert, err := tls.X509KeyPair(cert, pemkey)
		if err != nil {
			return nil, fmt.Errorf("wx pay tls.X509KeyPair failed")
		}
		resp, err = utils.DoPostWithCert("https://api.mch.weixin.qq.com"+api, "application/xml;charset=utf-8", str_req, time.Second*6, dCert)
		if err != nil {
			return nil, fmt.Errorf("wx pay do post failed err:%v", err)
		}
	}
	logger.Debugf("wx pay response:%s", string(resp))
	var wxPayResp utils.StringMap
	err = xml.Unmarshal(resp, &wxPayResp)
	if err != nil {
		return nil, err
	}
	if ok := CheckSign(wxPayResp, key); ok != ok {
		return nil, fmt.Errorf("wx pay do post failed. err:check sign failed.")
	}
	return wxPayResp, nil
}

func CheckSign(m utils.StringMap, key string) bool {
	sign := m["sign"]
	delete(m, "sign")
	if sign == wxGenerateSign(m, key) {
		return true
	}
	return false
}

type WXPay struct {
	AppId      string //应用ID
	AppSecret  string //应用秘钥
	MchId      string //支付商户ID
	Key        []byte //秘钥
	Cert       []byte //证书
	NotifyUrl  string //回调接口
	ExtraParam string //回传参数
}

//微信支付
func (wp *WXPay) Pay(amount float64, subject, body, outTradeNo, clientIp string) (string, string, error) {
	randStr, _ := utils.GenerateRandomString(32)
	m := utils.StringMap{
		"appid":            wp.AppId,
		"body":             subject,
		"detail":           body,
		"mch_id":           wp.MchId,
		"nonce_str":        randStr,
		"notify_url":       wp.NotifyUrl,
		"trade_type":       "MWEB",
		"spbill_create_ip": clientIp,
		"total_fee":        fmt.Sprintf("%d", int64(amount*100)),
		"out_trade_no":     outTradeNo,
		"time_start":       time.Now().Format("20060102150405"),
		"time_expire":      time.Now().Add(time.Minute * 10).Format("20060102150405"),
		"attach":           wp.ExtraParam,
	}
	resp, err := callWxPayAPI("/pay/unifiedorder", m, wp.AppSecret, "", nil, nil)
	if err != nil {
		return "", "", err
	}
	//检测返回参数
	if resp["return_code"] != "SUCCESS" {
		return "", "", fmt.Errorf("wxpay: result_code[%s] err_code[%s]", resp["return_code"], resp["return_msg"])
	}
	if resp["result_code"] != "SUCCESS" {
		return "", "", fmt.Errorf("wxpay: result_code[%s] err_code[%s] err_code_des[%s]", resp["result_code"], resp["err_code"], resp["err_code_des"])
	}
	//还需要验签检查业务错误
	return resp["prepay_id"], resp["mweb_url"], nil
}

//调用转账接口 返回微信单号 业务错误 外围错误
func (wp *WXPay) Transfer(amount float64, account, outTradeNo, desc, clientIp string) (string, string, error) {
	randStr, _ := utils.GenerateRandomString(32)
	m := utils.StringMap{
		"mch_appid":        wp.AppId,
		"mchid":            wp.MchId,
		"nonce_str":        randStr,
		"spbill_create_ip": clientIp,
		"amount":           fmt.Sprintf("%d", int64(amount*100)),
		"partner_trade_no": outTradeNo,
		"desc":             desc,
		"openid":           account,
		"check_name":       "NO_CHECK",
	}
	resp, err := callWxPayAPI("/mmpaymkttransfers/promotion/transfers", m, wp.AppSecret, wp.MchId, wp.Key, wp.Cert)
	if err != nil {
		return "", "", err
	}
	//检测返回参数
	if resp["return_code"] != "SUCCESS" {
		return "", "", fmt.Errorf("wxpay: result_code[%s] err_code[%s]", resp["return_code"], resp["return_msg"])
	}
	if resp["result_code"] != "SUCCESS" {
		return "", fmt.Sprintf("result_code[%s] err_code[%s] err_code_des[%s]", resp["result_code"], resp["err_code"], resp["err_code_des"]), nil
	}
	//还需要验签检查业务错误
	return resp["payment_no"], "SUCCESS", nil
}

func NewWXPay(extraParam, appId, appSecret, mchid string, key, cert []byte, notifyUrl string) *WXPay {
	return &WXPay{
		AppId:      appId,
		AppSecret:  appSecret,
		MchId:      mchid,
		Key:        key,
		Cert:       cert,
		NotifyUrl:  notifyUrl,
		ExtraParam: extraParam,
	}
}
