package yinlianpay

import (
	"encoding/json"
	"fmt"
	"gitlab.com/maoka/maoka-base/log"
	"gitlab.com/maoka/tool-kit/utils"
	"sort"
	"strings"
	"time"
)

var logger = log.GetLogger().WithField("package", "mkpay.yinlianpay")

func generateSign(m map[string]interface{}, key string) string {
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var signString string
	for _, k := range keys {
		value := fmt.Sprintf("%v", m[k])
		if value != "" && k != "sign" {
			if signString == "" {
				signString = fmt.Sprintf("%s=%s", k, value)
			} else {
				signString = fmt.Sprintf("%s&%s=%s", signString, k, value)
			}

		}
	}
	if key != "" {
		signString = fmt.Sprintf("%s%s", signString, key)
	}
	return strings.ToUpper(utils.GetMD5(signString))
}

//attachedData={merBillNo:2015090700000000}&billFunds=COUPON:200|PCARD:800&billFundsDesc=优惠金额2.00元，银行卡支付金额8.00元&buyerUsername=张三&invoiceAmount=800&merOrderId=2015090700000000&payTime=2015-09-07 16:00:00&status=SUCCESS&targetOrderId=2015090700000000&totalAmount=1000AAABBBCCCDDDEEEFFFGGG
func CheckSign(m map[string]interface{}, key string) bool {
	sign := m["sign"]
	delete(m, "sign")
	if sign == generateSign(m, key) {
		return true
	}
	return false
}

//调用银联接口
func callYinLianPayAPI(api string, m map[string]interface{}, key string) (string, map[string]interface{}, error) {
	m["sign"] = generateSign(m, key) //这个是计算yinlianpay签名的函数上面已贴出
	rb, err := json.Marshal(m)
	if err != nil {
		return "", nil, fmt.Errorf("json unmarshal failed. err:%v", err)
	}
	strReq := string(rb)
	logger.Debugf("yinlian pay request:%s", strReq)
	var resp []byte
	resp, err = utils.DoPost(api, "", strReq, time.Second*6)
	if err != nil {
		return "", nil, fmt.Errorf("yinlian pay do post failed err:%v", err)
	}
	logger.Debugf("yinlian pay response:%s", string(resp))
	var yinLianPayResp map[string]interface{}
	err = json.Unmarshal(resp, &yinLianPayResp)
	if err != nil {
		return "", nil, err
	}
	if ok := CheckSign(yinLianPayResp, key); ok != ok {
		return "", nil, fmt.Errorf("yinlian pay do post failed. err:check sign failed.")
	}
	return string(resp), yinLianPayResp, nil
}

type YinLianPay struct {
	Mid        string //商户ID 银联MID
	Tid        string //终端号
	InstMid    string //机构商户号
	MsgSrc     string //消息来源
	SignKey    string
	WxAppId    string //微信子商户appId
	NotifyUrl  string //通知接口
	ExtraParam string //回传参数
	MsgType    string //支付类型
}

func (yp *YinLianPay) Transfer(amount float64, account, outTradeNo, desc, clientIp string) (string, string, error) {
	return "", "", fmt.Errorf("yinlian pay no transfer api")
}

func (yp *YinLianPay) Pay(amount float64, subject, body, outTradeNo, clientIp string) (string, string, error) {
	m := map[string]interface{}{
		"msgSrc":           yp.MsgSrc,
		"msgType":          yp.MsgType,
		"requestTimestamp": time.Now().Format("2006-01-02 15:04:05"),
		"merOrderId":       outTradeNo,
		"mid":              yp.Mid,
		"tid":              yp.Tid,
		"instMid":          yp.InstMid,
		"attachedData":     yp.ExtraParam,
		"totalAmount":      amount * 100,
		"notifyUrl":        yp.NotifyUrl,
	}
	if yp.MsgType == "wx.unifiedOrder" || yp.MsgType == "wx.appPreOrder" {
		m["subAppId"] = yp.WxAppId
		m["tradeType"] = "APP"
	}
	resp, respMap, err := callYinLianPayAPI("https://qr.chinaums.com/netpay-route-server/api/", m, yp.SignKey)
	if err != nil {
		return "", "", err
	}
	//检测返回参数
	if respMap["errCode"] != "SUCCESS" {
		return "", "", fmt.Errorf("yinlianpay: resp:%s errCode[%s] errMsg[%s]", resp, respMap["errCode"], respMap["errMsg"])
	}
	return "", resp, nil
}

func NewYinLianPay(mid, tid, instMid, msgSrc, signKey, wxAppId, notifyUrl, extraParam, msgType string) *YinLianPay {
	return &YinLianPay{
		Mid:        mid,
		Tid:        tid,
		InstMid:    instMid,
		MsgSrc:     msgSrc,
		SignKey:    signKey,
		WxAppId:    wxAppId,
		NotifyUrl:  notifyUrl,
		ExtraParam: extraParam,
		MsgType:    msgType,
	}
}
