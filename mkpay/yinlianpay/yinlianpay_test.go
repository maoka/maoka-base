package yinlianpay

import (
	"fmt"
	"testing"
)

//buyerUsername=156****0251&payTime=2019-06-01+16%3A09%3A50&connectSys=UNIONPAY&sign=7D6EC1A9F38B680D3E9663D116693E9E&mid=898420679930105&invoiceAmount=1200&settleDate=2019-06-01&billFunds=%E6%94%AF%E4%BB%98%E5%AE%9D%E4%BD%99%E9%A2%9D%3A1200&buyerId=2088312910574035&mchntUuid=2d9081bc6af6c7fd016af6fb79615766&tid=51307631&receiptAmount=1200&couponAmount=0&attachedData=1001&targetOrderId=2019060122001474030597693669&billFundsDesc=%E6%94%AF%E4%BB%98%E5%AE%9D%E4%BD%99%E9%A2%9D%E6%94%AF%E4%BB%9812.00%E5%85%83%E3%80%82&orderDesc=%E6%B9%96%E5%8C%97%E5%92%96%E5%A8%B1%E4%BA%92%E5%8A%A8%E7%BD%91%E7%BB%9C%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8&seqId=08138174384N&merOrderId=54842019060116093516410011513781&targetSys=Alipay+2.0&totalAmount=1200&createTime=2019-06-01+16%3A09%3A35&RG=ZieU&buyerPayAmount=1200&notifyId=4d25dda1-709c-4e53-8bfd-c6d57878dfbc&subInst=102100&status=TRADE_SUCCESS
func TestYinLianPay_Pay(t *testing.T) {
	p := &YinLianPay{
		Mid:        "898310148160568",
		Tid:        "00000001",
		InstMid:    "APPDEFAULT",
		MsgSrc:     "WWW.TEST.COM",
		SignKey:    "fcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR",
		WxAppId:    "",
		NotifyUrl:  "https://www.baidu.com",
		ExtraParam: "",
	}
	_, resp, err := p.Pay(100, "测试商品", "测试商品100块", "31942020052712555111111111111114", "127.0.0.1")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(resp)
	//connectSys=ALIPAY&delegatedFlag=N&errCode=SUCCESS&merName=全渠道&merOrderId=319420200225061415075318399213&mid=898310148160568&msgSrc=WWW.TEST.COM&msgType=trade.precreate&qrCode=https://qr.alipay.com/bax05587zbf5jjkp1rsj4114&responseTimestamp=2020-02-26 16:42:53&seqId=00685500273N&settleRefId=00685500273N&status=NEW_ORDER&targetMid=2015061000120322&targetStatus=10000&targetSys=Alipay 2.0&tid=88880001fcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR
}
