package ams

import (
	"fmt"
	"os"

	base "gitlab.com/maoka/maoka-base/platformapi/protocol"
	ams "gitlab.com/maoka/maoka-base/platformapi/protocol/ams"
	mkutils "gitlab.com/maoka/maoka-base/utils"
	_ "gitlab.com/maoka/tool-kit/sign/rsasha1"
)

var productId uint32
var apiUrl string

func Init(product uint32, url string) {
	productId = product
	apiUrl = url
	if apiUrl == "" {
		fmt.Println("ams api: ams service url is empty")
		os.Exit(1)
	}
}

//获取玩家信息
func QueryUserInfoByUserId(userId string) (ams.UserInfo, error) {
	var req ams.QueryUserInfoReq
	var ack ams.QueryUserInfoAck
	req.SetProductID(productId)
	req.UserId = userId
	err := mkutils.DoSignPost(apiUrl+"/v1/role/queryuserinfo", "rsasha1", &req, &ack)
	if err != nil {
		return ack.Data, err
	}
	if ack.Result != ams.SUCCESS {
		return ack.Data, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	}
	return ack.Data, err
}

func QueryPromoterInfo(userId string) (*ams.QueryPromoterInfoAck, error) {
	var req ams.QueryPromoterInfoReq
	var ack ams.QueryPromoterInfoAck
	req.SetProductID(productId)
	req.UserId = userId
	err := mkutils.DoSignPost(apiUrl+"/v1/role/querypromoterinfo", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	// if ack.Result != ams.SUCCESS {
	// 	if ack.Result == ams.ERR_NOTBINDPROMOTER {
	// 		return "", &ack.BaseResponse, nil
	// 	}
	// 	return "", &ack.BaseResponse, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	// }
	// return "", ack.Data.PromoterId, err
	return &ack, nil
}

// 绑定上级 返回上级ID 错误码 请求错误
func BindPromoter(userId, promoterId string) (string, *base.BaseResponse, error) {
	var req ams.BindPromoterReq
	var ack ams.BindPromoterAck
	req.SetProductID(productId)
	req.UserId = userId
	req.PromoterId = promoterId
	err := mkutils.DoSignPost(apiUrl+"/v1/role/bindpromoter", "rsasha1", &req, &ack)
	if err != nil {
		return "", nil, err
	}
	return ack.Data.PromoterId, &ack.BaseResponse, nil
	// if ack.Result != ams.SUCCESS {
	// 	return "", ack.Data.PromoterId, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	// }
	// return "", ack.Data.PromoterId, err
}

func BindPhoneNumber(userId, phoneNumber string) (*ams.BindPhoneNumberAck, error) {
	var req ams.BindPhoneNumberReq
	var ack ams.BindPhoneNumberAck
	req.SetProductID(productId)
	req.UserId = userId
	req.PhoneNumber = phoneNumber
	err := mkutils.DoSignPost(apiUrl+"/v1/role/bindphonenumber", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	return &ack, nil
	// if ack.Result != ams.SUCCESS {
	// 	return "", ack.Data.PromoterId, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	// }
	// return "", ack.Data.PromoterId, err
}
func QueryThreeLayerPromoter(userId string) (*ams.QueryThreeLayerPromoterInfoAck, error) {
	var req ams.QueryThreeLayerPromoterInfoReq
	var ack ams.QueryThreeLayerPromoterInfoAck
	req.SetProductID(productId)
	req.UserId = userId
	err := mkutils.DoSignPost(apiUrl+"/v1/role/querythreelayerpromoterinfo", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	return &ack, nil
}

func BindPhonenum(userId, promoterId string) (string, *base.BaseResponse, error) {
	var req ams.BindPromoterReq
	var ack ams.BindPromoterAck
	req.SetProductID(productId)
	req.UserId = userId
	req.PromoterId = promoterId
	err := mkutils.DoSignPost(apiUrl+"/v1/role/bindpromoter", "rsasha1", &req, &ack)
	if err != nil {
		return "", nil, err
	}
	return ack.Data.PromoterId, &ack.BaseResponse, nil
	// if ack.Result != ams.SUCCESS {
	// 	return "", ack.Data.PromoterId, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	// }
	// return "", ack.Data.PromoterId, err
}
