package ams

import (
	"errors"
	"fmt"

	"gitlab.com/maoka/maoka-base/platformapi/protocol/ams"
	mkutils "gitlab.com/maoka/maoka-base/utils"
)

var ERRSENDSMSVERIFICATIONCODELIMIT = errors.New("ERRSENDSMSVERIFICATIONCODELIMIT") //验证码已发送请勿重复发送

//发送验证码
func SendSmsVerificationCode(userId, phoneNumber string) (*ams.SendVerificationCodeAck, error) {
	var req ams.SendVerificationCodeReq
	var ack ams.SendVerificationCodeAck
	req.SetProductID(productId)
	req.UserId = userId
	req.Phone = phoneNumber
	err := mkutils.DoSignPost(apiUrl+"/v1/smsverify/send", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	// if ack.Result != ams.SUCCESS {
	// 	if ack.Result == ams.ERR_SENDSMSVERIFICATIONCODELIMIT {
	// 		return ERRSENDSMSVERIFICATIONCODELIMIT
	// 	}
	// 	return fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	// }
	return &ack, nil
}

//检测验证码是否正确
func CheckSmsVerificationCode(phoneNumber, code string) (bool, error) {
	var req ams.CheckVerificationCodeReq
	var ack ams.CheckVerificationCodeAck
	req.SetProductID(productId)
	req.Phone = phoneNumber
	req.Code = code
	err := mkutils.DoSignPost(apiUrl+"/v1/smsverify/check", "rsasha1", &req, &ack)
	if err != nil {
		return false, err
	}
	if ack.Result != ams.SUCCESS {
		return false, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	}
	return ack.Data.Valid, nil
}
