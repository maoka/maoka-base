package ams

import (
	"fmt"

	ams "gitlab.com/maoka/maoka-base/platformapi/protocol/ams"
	mkutils "gitlab.com/maoka/maoka-base/utils"
)

// 检测token 返回游客标记 机器人标记 token是否有效
func CheckToken(userId, token string) (bool, bool, bool, error) {
	var req ams.CheckTokenReq
	var ack ams.CheckTokenAck
	req.SetProductID(productId)
	req.UserId = userId
	req.Token = token
	err := mkutils.DoSignPost(apiUrl+"/v1/token/checktoken", "rsasha1", &req, &ack)
	if err != nil {
		return false, false, false, err
	}
	if ack.Result != ams.SUCCESS {
		if ack.Result == ams.ERR_INVALIDTOKEN {
			return false, false, false, nil
		}
		return false, false, false, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	}
	return ack.Data.IsVisitor, ack.Data.IsRobot, ack.Data.Valid, nil
}

// 检测token 返回游客标记 机器人标记 token是否有效
func CheckTokenExt(userId, token string, verifyProductId uint32, checkTimestamp bool) (bool, bool, bool, error) {
	var req ams.CheckTokenReq
	var ack ams.CheckTokenAck
	req.SetProductID(productId)
	req.UserId = userId
	req.Token = token
	req.VerifyProductId = verifyProductId
	if !checkTimestamp {
		req.TimestampFlag = 1
	}
	err := mkutils.DoSignPost(apiUrl+"/v1/token/checktoken", "rsasha1", &req, &ack)
	if err != nil {
		return false, false, false, err
	}
	if ack.Result != ams.SUCCESS {
		if ack.Result == ams.ERR_INVALIDTOKEN {
			return false, false, false, nil
		}
		return false, false, false, fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	}
	return ack.Data.IsVisitor, ack.Data.IsRobot, ack.Data.Valid, nil
}

// 生成token 返回 用户ID token 错误
func GenerateToken(req *ams.GenerateTokenReq) (string, string, error) {
	req.SetProductID(productId)
	var ack ams.GenerateTokenAck
	err := mkutils.DoSignPost(apiUrl+"/v1/token/generatetoken", "rsasha1", req, &ack)
	if err != nil {
		return "", "", err
	}
	if ack.Result != ams.SUCCESS {
		return "", "", fmt.Errorf("ams api: response error code %d msg %s", ack.Result, ack.Msg)
	}
	return ack.Data.UserId, ack.Data.Token, nil
}
