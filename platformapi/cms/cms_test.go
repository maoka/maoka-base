package cms_test

import (
	"log"
	"testing"

	"gitlab.com/maoka/tool-kit/sign/keys"

	"gitlab.com/maoka/maoka-base/platformapi/cms"
)

var privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzGi5wcaVmRFeBPLTvizE5PyBcPnSv2DFhWcFfPNLsGuJ/lnY
s0nNGfUdKzlhvcDHguCtnxxV0X3aJHJa79P7yz4TXVyGh9kj43zQMh047ODZTpOC
a8qTVrXp8xsvGyCZxFLGxBqtERa6FUZjndOVLPmHFeF//7qDVBUeq4uNFY9+ZWa7
S6EeZaVNnNJF0ojJ49hR3tcocCYssv98WoV8RQ6v/qkO8JA/jnXu12gdP8pIvTTm
lX0gqJn9BQUeUmKC2h3tKNRhU+9541MRSspmHTqYTGTLMpK34/VBJKs/q+bT26zR
UdanXcOkAUsAbTwHJuPgX4B67kxZSksVeOy3yQIDAQABAoIBAEG7j7i7tpHRnu1y
dRsb5HlKpgmBnP8EDKC4n3AuAPILswWDOQd2vSkSxL2Ip2DQ5Je2zr5y6zoCAatk
xs+G2Nrj7HBCd9SpxDu8gGsNMKgkKl11XCRzsWvZ9hDCdiSAGFKk19xqQ6tSJqb+
yrwPNVKQL5uet32yKddZL1K0/rqmpfstYqwASJp5F0DkfCtKc/XusXBHNFeJyQgF
0ufg53nZpl9CnwWjguRWC7Uth2W2Hn7AIu8HESu5tt9T0ZzWdiptBGSFBDSu/E2N
uUrId71/zY3dfxGMXF4hkff1GaE/g7dJqzNy/91L/oN/UIjbplnFx1k3gSEV5Js/
gJ5WgHECgYEA8ornwp2P3cGryfF0u3ElfMbQDXndza7pASjmidO4MBRxHYgn6a93
d5abQPJ93HYeHHBcIVg+4b262MNAnEN5uky+IlZ7u5cXKA1qScnGlSaYNtZhnUtT
Wj7McAYcoX3Fp4ynGLXPuqQC/HXI7nnb8cYC6G3FSfWHAXa2s5xOEvUCgYEA18Ar
NPYWipIZDS/VJvipBcG28zQfbZkdlKmoSLCEcnsD0tsSAiJF7OjK5lCSZfeVERef
Z1DfcouOBzmPV0fE2i3Vx3mFkhl22f958F9uXT0LXg07IS9Mgo3fzKrIReQRJsi+
YzCPLMLv/nati09pX53IYZ/AlJU5pKF+7p/PVQUCgYBxA0pUQ2iKsrhiI3D5+hG2
n0jITrWMJmkJkk5BgHZ9aLkAQrYeTBVFktaKn7MHq74CEFTNcLn0I5zRbcFITqSE
q0eY5mVHFnLCxeV1tmAilVYtjxzy1utizIoj1KU4lksn9ADUIbtGQy/lBNj4Ta2v
bU1jkepwuMgMZY1iCX3TlQKBgEcLq+6zvraA56j1AF7l8VrI8ILjvhmsGxaSS60s
G2FLavXrV+HGAXrDKdVv4NWdEsRfIAm4kuw4UlvEs5QNOSCTiVs3hCmZIw8kVrY1
5rTjXjPpbXg/CBXRvEm8T1jcgl1gK3mkSvF1e+wyjnFahBuNbKfhIFyNSz8QQyk3
DOI1AoGBALxYO0PmPlouAY7nlykSXKVQ3aDUvcHqRm1WFANHRv2EmG/1OIstDFBZ
mn62WM1Hx+W79RY8oAbBuPXcDqXMUP95dMem/oUvEUn+PZYjyvT7H9PURW6fkCj7
33/oIg4CeOK0GBkkQG3m9cUBjL8mpct3kIS7CrMYbGs0s/xcjywb
-----END RSA PRIVATE KEY-----`
var publicKey = `-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAzGi5wcaVmRFeBPLTvizE5PyBcPnSv2DFhWcFfPNLsGuJ/lnYs0nN
GfUdKzlhvcDHguCtnxxV0X3aJHJa79P7yz4TXVyGh9kj43zQMh047ODZTpOCa8qT
VrXp8xsvGyCZxFLGxBqtERa6FUZjndOVLPmHFeF//7qDVBUeq4uNFY9+ZWa7S6Ee
ZaVNnNJF0ojJ49hR3tcocCYssv98WoV8RQ6v/qkO8JA/jnXu12gdP8pIvTTmlX0g
qJn9BQUeUmKC2h3tKNRhU+9541MRSspmHTqYTGTLMpK34/VBJKs/q+bT26zRUdan
XcOkAUsAbTwHJuPgX4B67kxZSksVeOy3yQIDAQAB
-----END RSA PUBLIC KEY-----`

func init() {
	cms.Init(10003, "http://127.0.0.1:1214")
	keys.RegisterKeys("private_10003", []byte(privateKey))
	keys.RegisterKeys("public_10003", []byte(publicKey))
}

func TestCurrencyUpdate(t *testing.T) {
	amount, balance, _, err := cms.CurrencyUpdate("ccb99a6e6bd07b4c64bceaa9b77d5557", 1, -100, 1006)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(amount, balance)
}

func TestQueryCurrencyBalance(t *testing.T) {
	balance, err := cms.QueryCurrencyBalance("ccb99a6e6bd07b4c64bceaa9b77d5557", 1)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(balance)
}

func TestCurrencyConvert(t *testing.T) {
	from, to, err := cms.CurrencyConvert("ccb99a6e6bd07b4c64bceaa9b77d5557", 1, 1000, 5, 1012)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(from, to)
}

func TestQueryRankList(t *testing.T) {
	list, err := cms.QueryRankList(1, 0, 20)
	if err != nil {
		t.Fatal(err)
	}
	for _, v := range list {
		log.Println(v)
	}

}

func TestQueryRankListRank(t *testing.T) {
	rank, err := cms.QueryRankListRank(1, 0, "ccb99a6e6bd07b4c64bceaa9b77d5557")
	if err != nil {
		t.Fatal(err)
	}
	log.Println(rank)
}

func TestBatchQueryCurrencyInfo(t *testing.T) {
	list, err := cms.BatchQueryCurrencyInfo("a44f05b8ee86e16e30631e536db57bf1", []uint32{1, 3})
	if err != nil {
		t.Fatal(err)
	}
	for _, v := range list {
		log.Println(v)
	}
}
