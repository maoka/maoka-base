package cms

import (
	"errors"
	"fmt"
	"os"

	"github.com/fabiolb/fabio/uuid"
	base "gitlab.com/maoka/maoka-base/platformapi/protocol"
	"gitlab.com/maoka/maoka-base/platformapi/protocol/cms"
	mkutils "gitlab.com/maoka/maoka-base/utils"
	_ "gitlab.com/maoka/tool-kit/sign/rsasha1"
)

var productId uint32
var apiUrl string

var ERRINSUFFICIENTBALANCE = errors.New("ERRINSUFFICIENTBALANCE")

func Init(product uint32, url string) {
	productId = product
	apiUrl = url
	if apiUrl == "" {
		fmt.Println("cms api: url is empty")
		os.Exit(1)
	}
}

//查看余额
func QueryCurrencyBalance(userId string, currencyId uint32) (int64, error) {
	var req cms.QueryBalanceReq
	var ack cms.QueryBalanceAck
	req.SetProductID(productId)
	req.UserId = userId
	req.CurrencyId = currencyId
	err := mkutils.DoSignPost(apiUrl+"/v1/currency/query", "rsasha1", &req, &ack)
	if err != nil {
		return ack.Data.Amount, err
	}
	if ack.Result != cms.SUCCESS {
		return ack.Data.Amount, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/currency/query", ack.Result, ack.Msg)
	}
	return ack.Data.Amount, nil
}

// 查看多种游戏币金额
func BatchQueryCurrencyInfo(userId string, currencyIdList []uint32) ([]cms.CurrencyItem, error) {
	var req cms.BatchQueryBalanceReq
	var ack cms.BatchQueryBalanceAck
	req.SetProductID(productId)
	req.UserId = userId
	req.CurrencyIdList = currencyIdList
	err := mkutils.DoSignPost(apiUrl+"/v1/currency/batchquery", "rsasha1", &req, &ack)
	if err != nil {
		return ack.Data.CurrencyList, err
	}
	if ack.Result != cms.SUCCESS {
		return ack.Data.CurrencyList, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/currency/batchquery", ack.Result, ack.Msg)
	}
	return ack.Data.CurrencyList, nil
}

//修改游戏币数量 返回修改游戏币数量，余额
func CurrencyUpdate(userId string, currencyId uint32, amount int64, transType uint32) (int64, int64, *base.BaseResponse, error) {
	var req cms.CurrencyUpdateReq
	var ack cms.CurrencyUpdateAck
	req.SetProductID(productId)
	req.UserId = userId
	req.CurrencyId = currencyId
	req.Amount = amount
	// req.Timestamp = time.Now().Format("2006/1/2 15:04:05")
	req.OrderId = uuid.NewUUID()
	req.TransactionType = transType
	err := mkutils.DoSignPost(apiUrl+"/v1/currency/update", "rsasha1", &req, &ack)
	if err != nil {
		return 0, 0, nil, err
	}
	// if ack.Result != cms.SUCCESS {
	// 	if ack.Result == cms.ERR_INSUFFICIENTBALANCE {
	// 		return ack.Data.Amount, ack.Data.Remain, ERRINSUFFICIENTBALANCE
	// 	}
	// 	return ack.Data.Amount, ack.Data.Remain, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/currency/update", ack.Result, ack.Msg)
	// }
	return ack.Data.Amount, ack.Data.Remain, &ack.BaseResponse, nil
}

//游戏币兑换
func CurrencyConvert(userId string, currencyId uint32, amount int64, convertTo uint32, transType uint32) (int64, int64, error) {
	var req cms.CurrencyConvertReq
	var ack cms.CurrencyConvertAck
	req.SetProductID(productId)
	req.UserId = userId
	req.ConvertFrom.CurrencyId = currencyId
	req.ConvertFrom.Amount = amount
	req.OrderId = uuid.NewUUID()
	req.ConvertTo = convertTo
	req.TransactionType = transType
	err := mkutils.DoSignPost(apiUrl+"/v1/currency/convert", "rsasha1", &req, &ack)
	if err != nil {
		return 0, 0, err
	}
	if ack.Result != cms.SUCCESS {
		if ack.Result == cms.ERR_INSUFFICIENTBALANCE {
			return ack.Data.ConvertFrom.Remain, ack.Data.ConvertTo.Remain, ERRINSUFFICIENTBALANCE
		}
		return ack.Data.ConvertFrom.Remain, ack.Data.ConvertTo.Remain, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/currency/convert", ack.Result, ack.Msg)
	}
	return ack.Data.ConvertFrom.Remain, ack.Data.ConvertTo.Remain, nil
}

func QueryRankList(currencyId uint32, rankType uint8, size int32) ([]cms.RichListItem, error) {
	var req cms.QueryRichListReq
	var ack cms.QueryRichListAck
	req.SetProductID(productId)
	req.RankType = rankType
	req.Size = size
	req.CurrencyId = currencyId
	err := mkutils.DoSignPost(apiUrl+"/v1/rank/richlist", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	if ack.Result != cms.SUCCESS {
		return nil, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/rank/richlist", ack.Result, ack.Msg)
	}
	return ack.Data.RankList, nil
}

func QueryRankListRank(currencyId uint32, rankType uint8, userId string) (uint32, error) {
	var req cms.QueryRichListRankReq
	var ack cms.QueryRichListRankAck
	req.SetProductID(productId)
	req.RankType = rankType
	req.UserId = userId
	req.CurrencyId = currencyId
	err := mkutils.DoSignPost(apiUrl+"/v1/rank/rank", "rsasha1", &req, &ack)
	if err != nil {
		return 0, err
	}
	if ack.Result != cms.SUCCESS {
		return 0, fmt.Errorf("cms api: %s response error code %d msg %s", apiUrl+"/v1/rank/rank", ack.Result, ack.Msg)
	}
	return ack.Data.Rank, nil
}

func UpdateDailyProfit(userId, date string, tax int64, promoterList []cms.ProfitItem) (*cms.UpdateDailyProfitAck, error) {
	var req cms.UpdateDailyProfitReq
	var ack cms.UpdateDailyProfitAck
	req.SetProductID(productId)
	req.Tax = tax
	req.UserId = userId
	req.PromoterList = promoterList
	req.Date = date
	err := mkutils.DoSignPost(apiUrl+"/v1/promoter/updatedailyprofit", "rsasha1", &req, &ack)
	if err != nil {
		return nil, err
	}
	return &ack, nil
}
