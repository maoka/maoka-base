package mkpay

import (
	"fmt"
	"gitlab.com/maoka/maoka-base/platformapi/protocol/mkpay"
	mkutils "gitlab.com/maoka/maoka-base/utils"
	"os"
)

var productId uint32
var apiUrl string

func Init(product uint32, url string) {
	productId = product
	apiUrl = url
	if apiUrl == "" {
		fmt.Println("mkpay api: mkpay service url is empty")
		os.Exit(1)
	}
}

func H5Pay(req *mkpay.MKPayReq, ack *mkpay.MKPayAck) error {
	req.SetProductID(productId)
	err := mkutils.DoSignPost(apiUrl+"/v1/unify/h5pay", "rsasha1", req, ack)
	if err != nil {
		return err
	}
	return nil
}
