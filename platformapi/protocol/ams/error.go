package ams

import (
	"fmt"
)

const (
	errorBase = 100
)

const (
	SUCCESS            = 0
	ERR_PARAMETERERROR = iota + errorBase
	ERR_INVALIDSIGN
	ERR_INVALIDCURRENCYTYPE
	ERR_INVALIDTRANSACTIONTYPE
	ERR_CURRENCYTYPELIMIT
	ERR_TRANSACTIONTYPELIMIT
	ERR_INVALIDORDERID
	ERR_SERVICEBUSY
	ERR_INSUFFICIENTBALANCE
	ERR_SENDSMSVERIFICATIONCODELIMIT
	ERR_SMSNOTCONFIG
	ERR_INVALIDTOKEN
	ERR_USERNOTEXIST
	ERR_DIFFRENTPRODUCT
	ERR_ALREADYBINDPROMOTER
	ERR_NOTBINDPROMOTER
	ERR_CANNOTBINDSELF
	ERR_CANNOTBINDVISITOR
	ERR_INVALIDPLATFORM
	ERR_ALREADYBINDPHONENUMBER
	ERR_PHONENUMBERALREADYEXIST
	ERR_NOTBINDPHONENUMBER
)

var errMap = make(map[uint32]string)

func registerError(code uint32, msg string) {
	if _, dup := errMap[code]; dup {
		fmt.Println(fmt.Sprintf("warn: dumplicate code %d msg:%s", code, msg))
	}
	errMap[code] = msg
}

func GetErrMsg(code uint32) string {
	if v, ok := errMap[code]; ok {
		return v
	}
	return "unknown error"
}

func init() {
	registerError(SUCCESS, "成功")
	registerError(ERR_PARAMETERERROR, "参数错误")
	registerError(ERR_INVALIDSIGN, "请求参数签名错误")
	registerError(ERR_INVALIDCURRENCYTYPE, "无效的货币类型")
	registerError(ERR_INVALIDTRANSACTIONTYPE, "无效的交易类型")
	registerError(ERR_CURRENCYTYPELIMIT, "超过游戏币规定最高交易额")
	registerError(ERR_TRANSACTIONTYPELIMIT, "超过交易渠道规定最高交易额")
	registerError(ERR_INVALIDORDERID, "订单号已存在")
	registerError(ERR_SERVICEBUSY, "服务器出错")
	registerError(ERR_INSUFFICIENTBALANCE, "余额不足")
	registerError(ERR_SENDSMSVERIFICATIONCODELIMIT, "验证码已发送请稍后再重新发送")
	registerError(ERR_SMSNOTCONFIG, "短信发送模板未配置")
	registerError(ERR_INVALIDTOKEN, "token验证失败")
	registerError(ERR_USERNOTEXIST, "玩家不存在")
	registerError(ERR_DIFFRENTPRODUCT, "两个账号是不同产品的账号")
	registerError(ERR_ALREADYBINDPROMOTER, "该账号已绑定上级")
	registerError(ERR_NOTBINDPROMOTER, "该账号未绑定上级")
	registerError(ERR_CANNOTBINDSELF, "不能绑定自己或自己下级账号")
	registerError(ERR_CANNOTBINDVISITOR, "不能绑定游客账号")
	registerError(ERR_INVALIDPLATFORM, "该账号不能进行此操作")
	registerError(ERR_ALREADYBINDPHONENUMBER, "账号已绑定手机号")
	registerError(ERR_PHONENUMBERALREADYEXIST, "手机号已被其他账号绑定")
	registerError(ERR_NOTBINDPHONENUMBER, "账号未绑定手机号")
}
