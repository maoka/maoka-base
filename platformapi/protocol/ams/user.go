package ams

import (
	base "gitlab.com/maoka/maoka-base/platformapi/protocol"
)

type BaseUserInfo struct {
	UserId      string `json:"userid"`      //玩家ID
	DisplayName string `json:"displayname"` //显示数字ID
	Sex         uint8  `json:"sex"`         //性别
	Nickname    string `json:"nickname"`    //昵称
	Imageurl    string `json:"imageurl"`    //头像
}

type UserInfo struct {
	BaseUserInfo
	Country     string `json:"country"`
	Province    string `json:"province"`
	City        string `json:"city"`
	Status      uint8  `json:"status"`
	KycFlag     uint8  `json:"kyc_flag"`     //实名认证标记 0未认证
	PhoneNumber string `json:"phone_number"` //手机号
}

// 根据第三方ID查询玩家信息
type QueryUserInfoByThirdReq struct {
	base.BaseParameter
	ThirdId       string `json:"thirdid"`  // 第三方ID
	ThirdPlatform int    `json:"platform"` // 来源平台ID
}

type QueryUserInfoByThirdAck struct {
	base.BaseResponseWithSign
	Data UserInfo `json:"data"`
}

// 登录获取token 只能登录服务使用
type GenerateTokenReq struct {
	base.BaseParameter
	Thirdid  string `json:"thirdid"`
	Platform uint32 `json:"platform"`

	Nickname string `json:"nickname"`
	Sex      int    `json:"sex"`
	Imageurl string `json:"imageurl"`
	Country  string `json:"country"`
	Province string `json:"province"`
	City     string `json:"city"`
	// Introid     string `json:"introid"`
	Channelinfo string `json:"channelinfo"`

	Device       string `json:"device"`     //设备信息
	ClientIp     string `json:"client_ip"`  //客户端IP
	LoginType    int    `json:"login_type"` //登录方式
	LoginProduct uint32 `json:"login_product"`

	//Idno     string `json:"idno",omitempty`
	Phonenum  string `json:"phonenum"`
	IsVisitor bool   `json:"is_visitor"` //是不是游客 0不是
	IsRobot   bool   `json:"is_robot"`   //是不是机器人 0不是
}

type GenerateTokenAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId string `json:"userid"`
		Token  string `json:"token"`
	} `json:"data"`
}

//检测token
type CheckTokenReq struct {
	base.BaseParameter
	UserId          string `json:"userid"`
	Token           string `json:"token"`
	VerifyProductId uint32 `json:"verify_productid"` //用于验签的产品ID，非必填，不填则用基础参数中的产品ID验签
	TimestampFlag   int    `json:"timestamp_flag"`   //是否检测时间戳是否有效 0检测 1不检测
}
type CheckTokenAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId    string `json:"userid"`
		Token     string `json:"token"`
		Valid     bool   `json:"valid"`
		IsVisitor bool   `json:"is_visitor"` //是不是游客 0不是
		IsRobot   bool   `json:"is_robot"`   //是不是机器人 0不是
	} `json:"data"`
}

//查询玩家信息
type QueryUserInfoReq struct {
	base.BaseParameter
	UserId      string `json:"userid"`
	DisplayName string `json:"displayname"`
}
type QueryUserInfoAck struct {
	base.BaseResponseWithSign
	Data UserInfo `json:"data"`
}

//查询玩家信息 两个都是非必填
type QueryBaseUserInfoReq struct {
	base.BaseParameter
	UserId      string `json:"userid"`
	DisplayName string `json:"displayname"`
}
type QueryBaseUserInfoAck struct {
	base.BaseResponseWithSign
	Data BaseUserInfo `json:"data"`
}

type BatchQueryBaseUserInfoReq struct {
	base.BaseParameter
	UserList []string `json:"user_list"` //玩家id数组
}
type BatchQueryBaseUserInfoAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserList []BaseUserInfo `json:"user_list"` //玩家基本信息列表
	}
}

//发送验证码请求
type SendVerificationCodeReq struct {
	base.BaseParameter
	UserId string `json:"userid"` //玩家ID
	Phone  string `json:"phone"`  //手机号
}
type SendVerificationCodeAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId string `json:"userid"` //玩家ID
		Phone  string `json:"phone"`  //手机号
	} `json:"data"`
}

//发送验证码请求 (UserId不为空绑定手机号到玩家信息)
type CheckVerificationCodeReq struct {
	base.BaseParameter
	// UserId       string `json:"userid"` //玩家ID
	Phone string `json:"phone"` //手机号
	Code  string `json:"code"`  //验证码
}
type CheckVerificationCodeAck struct {
	base.BaseResponseWithSign
	Data struct {
		// UserId string `json:"userid"` //玩家ID
		Phone string `json:"phone"` //手机号
		Valid bool   `json:"valid"`
	} `json:"data"`
}

type QueryPromoterInfoReq struct {
	base.BaseParameter
	UserId string `json:"userid"` //玩家ID
}
type QueryPromoterInfoAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId      string `json:"userid"`      //玩家ID
		PromoterId  string `json:"promoterid"`  //上级ID
		DisplayName string `json:"displayname"` //上级数字ID
		NickName    string `json:"nickname"`    //上级昵称
		ImageURL    string `json:"imageurl"`    //上级头像
		BindType    uint8  `json:"bind_type"`   //绑定上级的渠道
	} `json:"data"`
}

type QueryThreeLayerPromoterInfoReq struct {
	base.BaseParameter
	UserId string `json:"userid"` //玩家ID
}
type QueryThreeLayerPromoterInfoAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId    string `json:"userid"`    //玩家ID
		Promoter1 string `json:"promoter1"` //上级ID
		Promoter2 string `json:"promoter2"` //上上级ID
		Promoter3 string `json:"promoter3"` //上上上级ID
	} `json:"data"`
}
type BindPromoterReq struct {
	base.BaseParameter
	UserId     string `json:"userid"`     //玩家ID
	PromoterId string `json:"promoterid"` //上级ID或数字ID
	BindType   uint8  `json:"bind_type"`  //绑定上级的渠道
}
type BindPromoterAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId     string `json:"userid"`     //玩家ID
		PromoterId string `json:"promoterid"` //上级ID
	} `json:"data"`
}

type BindPhoneNumberReq struct {
	base.BaseParameter
	UserId      string `json:"userid"`       //玩家ID
	PhoneNumber string `json:"phone_number"` //电话号码
}
type BindPhoneNumberAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId      string `json:"userid"`       //玩家ID
		PhoneNumber string `json:"phone_number"` //电话号码
	} `json:"data"`
}
