package protocol

type IBaseParameter interface {
	GetProductID() uint32
	GetSign() string
	SetProductID(uint32)
	SetSign(string)
}
type BaseParameter struct {
	ProductID uint32 `json:"productid"`
	Sign      string `json:"sign"`
}

func (r *BaseParameter) GetProductID() uint32 {
	return r.ProductID
}
func (r *BaseParameter) GetSign() string {
	return r.Sign
}
func (r *BaseParameter) SetProductID(p uint32) {
	r.ProductID = p
}
func (r *BaseParameter) SetSign(sign string) {
	r.Sign = sign
}

type IBaseResponse interface {
	SetCode(uint32)
	SetErrMsg(string)
	GetCode() uint32
	GetErrMsg() string
}

// 返回信息
type BaseResponse struct {
	Result uint32 `json:"result"` //错误码
	Msg    string `json:"msg"`    //消息内容
}

func (r *BaseResponse) SetCode(code uint32) {
	r.Result = code
}
func (r *BaseResponse) SetErrMsg(msg string) {
	r.Msg = msg
}
func (r *BaseResponse) GetCode() uint32 {
	return r.Result
}
func (r *BaseResponse) GetErrMsg() string {
	return r.Msg
}

//带签名的返回消息
type IBaseResponseWithSign interface {
	IBaseResponse
	IBaseParameter
}

type BaseResponseWithSign struct {
	BaseResponse
	BaseParameter
}
