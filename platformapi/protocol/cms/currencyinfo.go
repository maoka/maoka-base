package cms

import base "gitlab.com/maoka/maoka-base/platformapi/protocol"

// 查询玩家所有游戏币类型余额（传一个类型列表）
type CurrencyItem struct {
	CurrencyId uint32 `json:"currencyid"`
	Amount     int64  `json:"amount"`
}
type CurrencyItemExt struct {
	CurrencyId uint32 `json:"currencyid"`
	Amount     int64  `json:"amount"`
	Remain     int64  `json:"remain"`
}

// 批量查余额
type BatchQueryBalanceReq struct {
	base.BaseParameter
	UserId         string   `json:"userid"`
	CurrencyIdList []uint32 `json:"currencyid_list"` //要查询的物品类型列表
}
type BatchQueryBalanceAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId       string         `json:"userid"`
		CurrencyList []CurrencyItem `json:"currency_list"` //查询结果列表
	} `json:"data"`
}

// 查余额
type QueryBalanceReq struct {
	base.BaseParameter
	UserId     string `json:"userid"`
	CurrencyId uint32 `json:"currencyid"` //要查询的物品类型
}
type QueryBalanceAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId string `json:"userid"`
		CurrencyItem
	} `json:"data"`
}

type BillingInfo struct {
	OrderId string `json:"orderid"` //订单号
	// Timestamp       string `json:"timestamp"`
	TransactionType uint32 `json:"transaction_type"`
}

type CurrencyUpdateReq struct {
	base.BaseParameter
	BillingInfo
	UserId       string `json:"userid"`
	CurrencyItem        //要改变的物品信息 id 和要修改量
}

type CurrencyUpdateAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId  string `json:"userid"`
		OrderId string `json:"orderid"`
		CurrencyItemExt
	} `json:"data"`
}

//TODO:物品兑换
type CurrencyConvertReq struct {
	base.BaseParameter
	BillingInfo
	UserId      string       `json:"userid"`
	ConvertFrom CurrencyItem `json:"convert_from"` //用什么物品兑换 消耗多少
	ConvertTo   uint32       `json:"convert_to"`   //想兑换的游戏币ID
}
type CurrencyConvertAck struct {
	base.BaseResponseWithSign
	Data struct {
		OrderId     string          `json:"orderid"` //订单号
		UserId      string          `json:"userid"`
		ConvertFrom CurrencyItemExt `json:"convert_from"`
		ConvertTo   CurrencyItemExt `json:"convert_to"`
	}
}

// 检查订单号是否存在
type CheckOrderIdExistReq struct {
	base.BaseParameter
	UserId  string `json:"userid"`
	OrderId string `json:"orderid"` //订单号
}
type CheckOrderIdExistAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId  string `json:"userid"`
		OrderId string `json:"orderid"` //订单号
		Exist   bool   `json:"exist"`   //true 已有订单存在
	} `json:"data"`
}
type RichListItem struct {
	UserId string `json:"userid"`
	Amount int64  `json:"amount"`
}

//财富排行榜
type QueryRichListReq struct {
	base.BaseParameter
	CurrencyId uint32 `json:"currencyid"` //游戏币ID
	RankType   uint8  `json:"ranktype"`   //排行榜类型 0本产品 1所有游戏
	Size       int32  `json:"size"`       //排行榜数量
}
type QueryRichListAck struct {
	base.BaseResponseWithSign
	Data struct {
		CurrencyId uint32         `json:"currencyid"` //游戏币ID
		RankType   uint8          `json:"ranktype"`   //排行榜类型 0本产品 1所有游戏
		RankList   []RichListItem `json:"rank_list"`  //名次
	} `json:"data"`
}

//查某个人的排名
type QueryRichListRankReq struct {
	base.BaseParameter
	UserId     string `json:"userid"`
	CurrencyId uint32 `json:"currencyid"` //游戏币ID
	RankType   uint8  `json:"ranktype"`   //排行榜类型 0本产品 1所有游戏
}
type QueryRichListRankAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId     string `json:"userid"`
		CurrencyId uint32 `json:"currencyid"` //游戏币ID
		RankType   uint8  `json:"ranktype"`   //排行榜类型 0本产品 1所有游戏
		Rank       uint32 `json:"rank"`       //名次
	} `json:"data"`
}
