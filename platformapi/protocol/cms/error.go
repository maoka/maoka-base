package cms

import (
	"fmt"
)

const (
	errorBase = 200
)

const (
	SUCCESS            = 0
	ERR_PARAMETERERROR = iota + errorBase
	ERR_INVALIDSIGN
	ERR_INVALIDCURRENCYTYPE
	ERR_INVALIDTRANSACTIONTYPE
	ERR_CURRENCYTYPELIMIT
	ERR_TRANSACTIONTYPELIMIT
	ERR_INVALIDORDERID
	ERR_SERVICEBUSY
	ERR_INSUFFICIENTBALANCE
	ERR_CONVERTRATENOTCONFIG
	ERR_INVALIDCURRENCYAMOUNT
	ERR_NOTALLOWEDUPDATE
)

var errMap = make(map[uint32]string)

func registerError(code uint32, msg string) {
	if _, dup := errMap[code]; dup {
		fmt.Println(fmt.Sprintf("warn: dumplicate code %d msg:%s", code, msg))
	}
	errMap[code] = msg
}

func GetErrMsg(code uint32) string {
	if v, ok := errMap[code]; ok {
		return v
	}
	return "unknown error"
}

func init() {
	registerError(SUCCESS, "成功")
	registerError(ERR_PARAMETERERROR, "参数错误")
	registerError(ERR_INVALIDSIGN, "请求参数签名错误")
	registerError(ERR_INVALIDCURRENCYTYPE, "无效的货币类型")
	registerError(ERR_INVALIDTRANSACTIONTYPE, "无效的交易类型")
	registerError(ERR_CURRENCYTYPELIMIT, "超过游戏币规定最高交易额")
	registerError(ERR_TRANSACTIONTYPELIMIT, "超过交易渠道规定最高交易额")
	registerError(ERR_INVALIDORDERID, "订单号已存在")
	registerError(ERR_INSUFFICIENTBALANCE, "余额不足")
	registerError(ERR_SERVICEBUSY, "服务器出错")
	registerError(ERR_CONVERTRATENOTCONFIG, "游戏币兑换比例未配置")
	registerError(ERR_INVALIDCURRENCYAMOUNT, "无效的游戏币交易数量")
	registerError(ERR_NOTALLOWEDUPDATE, "禁止交易")
}
