package cms

import base "gitlab.com/maoka/maoka-base/platformapi/protocol"

type ProfitItem struct {
	Level    uint8  `json:"level"`    //推官员层级
	Promoter string `json:"promoter"` //推广员ID
}
type UpdateDailyProfitReq struct {
	base.BaseParameter
	UserId       string       `json:"userid"`        //玩家ID
	Tax          int64        `json:"tax"`           //台费
	Date         string       `json:"date"`          //统计日期
	PromoterList []ProfitItem `json:"promoter_list"` //需要返利的推广员
}
type UpdateDailyProfitAck struct {
	base.BaseResponseWithSign
	Data struct {
		UserId string `json:"userid"` //玩家ID
	} `json:"data"`
}
