package mkpay

import (
	"fmt"
)

const (
	errorBase = 300
)

const (
	SUCCESS            = 0
	ERR_PARAMETERERROR = iota + errorBase
	ERR_INVALIDSIGN
	ERR_SERVICEBUSY
	ERR_BACKENDBUSY
	ERR_COMMODITYNOTEXIST
	ERR_UNIFIEDORDERFAILED
	ERR_ERRPAYINFO
	ERR_DUPLICATETRADENO
	ERR_PAYFAILED
)

var errMap = make(map[uint32]string)

func registerError(code uint32, msg string) {
	if _, dup := errMap[code]; dup {
		fmt.Println(fmt.Sprintf("warn: dumplicate code %d msg:%s", code, msg))
	}
	errMap[code] = msg
}

func GetErrMsg(code uint32) string {
	if v, ok := errMap[code]; ok {
		return v
	}
	return "unknown error"
}

func init() {
	registerError(SUCCESS, "成功")
	registerError(ERR_PARAMETERERROR, "参数错误")
	registerError(ERR_INVALIDSIGN, "请求参数签名错误")
	registerError(ERR_SERVICEBUSY, "服务器繁忙")
	registerError(ERR_BACKENDBUSY, "接口服务异常")
	registerError(ERR_COMMODITYNOTEXIST, "商品不存在")
	registerError(ERR_UNIFIEDORDERFAILED, "调用支付接口下单失败")
	registerError(ERR_ERRPAYINFO, "支付信息有误")
	registerError(ERR_DUPLICATETRADENO, "订单号重复")
	registerError(ERR_PAYFAILED, "支付失败")
}
