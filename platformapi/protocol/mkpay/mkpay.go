package mkpay

import base "gitlab.com/maoka/maoka-base/platformapi/protocol"

type MKPayReq struct {
	base.BaseParameter
	PayType    uint8  `json:"paytype"`    //支付类型
	ChannelId  string `json:"channelid"`  //支付通道ID
	Amount     string `json:"amount"`     //支付金额 "1.00"
	Subject    string `json:"subject"`    //商品标题
	Body       string `json:"body"`       //商品描述信息
	TradeNo    string `json:"tradeno"`    //订单号
	ClientIP   string `json:"clientip"`   //下单客户端IP （微信必须是真实的IP否则支付失败）
	NotifyUrl  string `json:"notifyurl"`  //支付完成后异步通知地址(无需编码)
	ExtraParam string `json:"extraparam"` //异步回传参数
	//ReturnUrl   string `json:"returnurl"`   //支付完成后跳转地址
}

type MKPayAck struct {
	base.BaseResponseWithSign
	Data struct {
		TradeSeq string `json:"tradeseq"` //平台订单号 用于调试
		TradeNo  string `json:"tradeno"`  //订单号
		PayUrl   string `json:"payurl"`   //支付跳转链接
	} `json:"data"`
}

type MKNotifyAck struct {
	base.BaseResponseWithSign
	Data struct {
		TradeSeq   string `json:"tradeseq"`   //平台订单号
		TradeNo    string `json:"tradeno"`    //订单号
		Amount     string `json:"amount"`     //支付金额 "1.00"
		PayType    uint8  `json:"paytype"`    //支付类型
		ExtraParam string `json:"extraparam"` //异步回传参数
	} `json:"data"`
}
