package service_test

import (
	"gitlab.com/maoka/maoka-base/service"
	"gitlab.com/maoka/tool-kit/peer"
	_ "gitlab.com/maoka/tool-kit/peer/websocket"
	"log"
	"testing"
	"time"
)

type TestMessage struct {
	Id      int    `json:"id"`
	Content string `json:"content"`
}
type TestService struct {
	service.ComponentBase
}

func (s *TestService) Init() {
	log.Println("call init function.")
}

func (s *TestService) OnSessionClose(session *peer.Session) bool {
	log.Printf("session closed : %d", session.Conn.ID())
	return true
}

func (s *TestService) TestHandler(session *peer.Session, message *TestMessage) (*TestMessage, error) {
	log.Println(message.Id, message.Content) //w
	return message, session.Conn.Send([]byte(time.Now().String() + " response content:" + message.Content))
}

func (s *TestService) TestFunc(session *peer.Session, message *TestMessage) (*TestMessage, error) {
	log.Println(message.Id, message.Content) //w
	return message, session.Conn.Send([]byte(time.Now().String() + " func response content:" + message.Content))
}

func TestNewService(t *testing.T) {
	s := new(TestService)
	service.RegisterService(s)
	svr := peer.GetAcceptor("ws")
	err := svr.Start("ws://:8080/echo", service.GetSessionManager())
	if err != nil {
		log.Fatal(err)
	}
}
