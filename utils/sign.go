package utils

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/robfig/config"
	base "gitlab.com/maoka/maoka-base/platformapi/protocol"
	"gitlab.com/maoka/tool-kit/sign"
	"gitlab.com/maoka/tool-kit/sign/keys"
	"gitlab.com/maoka/tool-kit/utils"
)

func SignObject(signObj sign.Sign, obj base.IBaseParameter) ([]byte, error) {
	keyName := fmt.Sprintf("private_%d", obj.GetProductID())
	privateKey := keys.GetKey(keyName)
	if privateKey == nil {
		return nil, fmt.Errorf("sign: privatekey(%s) not register", keyName)
	}
	ab, err := sign.Object2OrderJson(&obj)
	if err != nil {
		return nil, fmt.Errorf("sign: sign.OrderJsonString failed %v", err)
	}
	s, err := signObj.Sign(ab, privateKey)
	if err != nil {
		return nil, fmt.Errorf("sign: signObj.Sign failed %v", err)
	}
	obj.SetSign(s)
	jab, err := json.Marshal(obj)
	if err != nil {
		return nil, fmt.Errorf("sign: json.Marshal failed %v", err)
	}
	return jab, nil
}

func SignVerify(signObj sign.Sign, data []byte) error {
	ab, err := sign.Json2OrderJson(data)
	if err != nil {
		return fmt.Errorf("sign: sign.Json2OrderJson failed %v", err)
	}
	var obj base.BaseParameter
	err = json.Unmarshal(data, &obj)
	if err != nil {
		return fmt.Errorf("sign: json unmarshal to base.BaseParameter failed %v", err)
	}
	keyName := fmt.Sprintf("public_%d", obj.GetProductID())
	publicKey := keys.GetKey(keyName)
	if publicKey == nil {
		return fmt.Errorf("sign: publickey(%s) not register ", keyName)
	}
	err = signObj.Verify(ab, publicKey, obj.GetSign())
	if err != nil {
		return fmt.Errorf("sign: signObj.Verify failed %v", err)
	}
	return nil
}

//带签名POST请求
//默认超时时间三秒，只有这里能改
func DoSignPost(url, signName string, req, ack base.IBaseParameter) error {
	var signObj = sign.GetSignObject(signName)
	if signObj == nil {
		return fmt.Errorf("sign: %s sign function not register", signName)
	}
	//签名
	jab, err := SignObject(signObj, req)
	if err != nil {
		return fmt.Errorf("sign: utils.SignObject failed %v", err)
	}
	resp, err := utils.DoPost(url,
		"", string(jab), time.Second*3)
	if err != nil {
		return fmt.Errorf("sign: utils.DoPost failed %v", err)
	}
	//验签
	err = SignVerify(signObj, resp)
	if err != nil {
		return fmt.Errorf("sign: utils.SignVerify failed resp(%s) err(%v)", string(resp), err)
	}
	err = json.Unmarshal(resp, ack)
	if err != nil {
		return fmt.Errorf("sign: unmarshal failed resp(%s) err(%v)", string(resp), err)
	}
	return nil
}

// 在配置文件keys section下面配置秘钥文件路径 格式如下：
// 私钥：private_productid eg: private_1001: pem_path
// 公钥: public_productid eg: public_1001: pem_path
func RSAKeyRegister(conf *config.Config) error {
	options, err := conf.SectionOptions("keys")
	if err != nil {
		return fmt.Errorf("keyregister: load keys options failed %v", err)
	}
	for _, v := range options {
		pemFileName, err := conf.String("keys", v)
		if err != nil {
			return fmt.Errorf("keyregister: load keys(%s) failed err:%v", v, err)
		}
		key, err := utils.LoadFile(pemFileName)
		if err != nil {
			return fmt.Errorf("keyregister: load pem file(%s) failed err:%v", pemFileName, err)
		}
		keys.RegisterKeys(v, key)
		fmt.Printf("keyregister: load pem(%s) succ\n", v)
	}
	return nil
}
